import { Navigate } from 'react-router-dom';
import { useEffect, useContext } from 'react';
import UserContext from '../UserContext';


export default function Logout() {
	// consumes the UserContext object and destructure it to access the unsetUser and setuser states
	const {unsetUser, setUser} = useContext(UserContext);

	// clear the localStorage
	unsetUser();
	
	/*
		placing the "setUser" setter function inside a useEffect is necessary because of the updates in ReactJS that a state of another component cannot be updated while trying to render a different component

		by adding useEffect, this will render Logout page fisrt before trigeering the useEffect which changes the state of the user
	*/
	useEffect(()=>{
		// set the user state back to it's original state
		setUser({id: null})
	})



	return(
		<Navigate to='/login' />
	)
}
