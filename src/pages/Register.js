import{Form, Button} from "react-bootstrap";
import {useState, useEffect, useContext} from "react";
import {Navigate} from 'react-router-dom';
// User COntext
import UserContext from '../UserContext';
import Swal from "sweetalert2";

export default function Register(){

	const {user} = useContext(UserContext)

	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [mobileNo, setMobileNo] = useState("")
	const [email, setEmail] = useState("")
	const [password1, setPassword1] = useState("")
	const [password2, setPassword2] = useState("")
	const [isActive, setIsActive] = useState(false)

/*
	whenver a state of a component changes, the component renders the whole component executing the code found in the component itself
	e.target.value allows us to gain access the input field's current value to be used when submitting from data
*/	
	console.log(firstName);
	console.log(lastName);
	console.log(mobileNo);
	console.log(email);
	console.log(password1);
	console.log(password2);

	useEffect(()=>{
		if((firstName!== " " && lastName!=="" && mobileNo!=="" && email!=="" && password1 !== " "&& password2!== " ")&&(password1===password2)&&(mobileNo.length >= 11)){
			setIsActive(true)
			
		}else{
			setIsActive(false)
		
		}
	}, [firstName, lastName, mobileNo, email, password1, password2]);

	function registerUser(e){
		e.preventDefault();

		fetch('http://localhost:4000/users/register',{
			method: 'POST',
			headers:{
				'Content-type':'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				mobileNo: mobileNo,
				email: email,
				password1: password1,
				password1: password2
			})
		})

		.then(res => res.json())
		.then(data => {
			console.log(data)

		})

		
		// clear our input fields
		setFirstName(" ")
		setLastName(" ")
		setMobileNo(" ")
		setEmail("");
		setPassword1("");
		setPassword2("");


		alert("Thank you for registering!")
	}

	return (
		// (user.email !== null)?
		// <Navigate to='/courses'/>
		// :
		    <Form onSubmit = {(e)=>registerUser(e)}>
		      <Form.Group controlId="userFname">
		        <Form.Label>First Name</Form.Label>
		        <Form.Control 
		        	type="fName" 
		        	placeholder="Enter First Name" 
		        	value={firstName}
		        	onChange = {e=>setFirstName(e.target.value)}
		        	required/>
	
		      </Form.Group>
		      <Form.Group controlId="userLname">
		        <Form.Label>Last Name</Form.Label>
		        <Form.Control 
		        	type="lName" 
		        	placeholder="Enter Last Name" 
		        	value={lastName}
		        	onChange = {e=>setLastName(e.target.value)}
		        	required/>
		 
		      </Form.Group>

		      <Form.Group controlId="userMobileNo">
		        <Form.Label>Mobile No.</Form.Label>
		        <Form.Control 
		        	type="mobileNo" 
		        	placeholder="Enter Mobile Number" 
		        	value={mobileNo}
		        	onChange = {e=>setMobileNo(e.target.value)}
		        	required/>
		        
		      </Form.Group>
		      <Form.Group controlId="userEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control 
		        	type="email" 
		        	placeholder="Enter email" 
		        	value={email}
		        	onChange = {e=>setEmail(e.target.value)}
		        	required/>
		        <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        </Form.Text>
		      </Form.Group>

		      <Form.Group controlId="password1">
		        <Form.Label>Password</Form.Label>
		        <Form.Control 
		        	type="password" 
		        	placeholder="Password"
		        	value={password1}
		        	onChange = {e=>setPassword1(e.target.value)}
		        	required
		         />
		      </Form.Group>

		      <Form.Group controlId="password2">
		        <Form.Label>Verify Password</Form.Label>
		        <Form.Control 
		        	type="password" 
		        	placeholder="Password" 
		        	value={password2}
		        	onChange = {e=>setPassword2(e.target.value)}
		        	required
		        />
		      </Form.Group>
		      {isActive?
		      <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
		      :
		       <Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
		   		}
		    </Form>
	  );
}