import {Fragment, useEffect, useState} from 'react';
// import coursesData from "../data/coursesData";
import CourseCard from '../components/CourseCard';

/*
	the course in our CourseCard component can be sent 	as a prop 
		prop - is a shorthand of "property" since the components are considered as an object in ReactJs
	the Curly Braces are used for props to signify that we are providing information using JS expressions rather than hard coded values which use double qoutes
*/
export default function Courses(){
	// console.log(coursesData);

	const [courses, setCourses] = useState([]);

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
		.then(res=>res.json())
		.then(data=>{
			console.log(data)

			setCourses(data.map(course=>{
				return(
					<CourseCard key={course._id} courseProp = {course}/>
				);
			}));
		})
	}, [])

	/*
		the map method loops through individual course objects and returns a component for each course
		mulitple components created through map method must have a unique key that will help
		ReactJS identify which component/elements have been changed added or removed
	*/
	/*const courses = coursesData.map(course=>{
		return(
			<CourseCard key={course.id} courseProp = {course}/>

			)
	})
*/
	return(
		<Fragment>
			{courses}
		</Fragment>

		)
}