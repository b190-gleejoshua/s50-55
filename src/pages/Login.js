import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
// User COntext
import UserContext from '../UserContext';

// sweetalert2
import Swal from 'sweetalert2';

export default function Login() {

	const {user, setUser} = useContext(UserContext)

	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');

	const [ isActive, setIsActive] = useState(false);


	const retrieveUserDetails = (token) =>{
		fetch('http://localhost:4000/users/details',{
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res =>res.json())
		.then(data => {
			console.log(data);


			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}
	useEffect(()=>{
		if (email !== ''&& password !== '') {
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[email, password]);

	function loginUser(e){
		e.preventDefault();

		// fetch method will communicate with our backend application it with a stringified JSON coming from the body
		/*
			SYNTAX:
				fetch('url', {options})
				.then(res=> res.json())
				.then(data => {})


				.then is used to create a promise chain to catch the data from  the fetch method
		*/
		fetch('http://localhost:4000/users/login',{
			method: 'POST',
			headers:{
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})

		.then(res => res.json())
		// good practice to output the response in the console to check if the codes are working
		.then(data => {
			console.log(data)

			if (typeof data.access !== 'undefined'){
				// JWT will be used to  retrieve user information accross the whole frontenD application
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title: "Login sucessful!",
					icon: "success",
					text: "Welcome to Zuitt!"

					})
			}else{
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your login credentials and try again"
				})
			}

		});

		/*
			localStorage.setItem allows us to manipulate the browser's local storage to store information indefinitely to help demonstrate the condition rendering	
			localStorage does not trigeer rerendering of components; so for if to be able to take effect, we have to refresh the browser
		*/
		/*localStorage.setItem('email', email);

		setUser({
			email: localStorage.getItem('email')
		});*/

		setEmail('');
		setPassword('');

	}


	return(
		(user.id !== null)?
		<Navigate to='/courses'/>
		:
		  <Form onSubmit={(e) => loginUser(e)}>
		    <Form.Group controlId="userEmail">
		      <Form.Label>Email address</Form.Label>
		      <Form.Control 
		      				type="email" 
		      				placeholder="Enter email" 
		      				value={email}
		      				onChange={e => setEmail(e.target.value)}
		      				required 
				/>
		      <Form.Text className="text-muted">
		        We'll never share your email with anyone else.
		      </Form.Text>
		    </Form.Group>

		    <Form.Group controlId="password1">
		      <Form.Label>Password</Form.Label>
		      <Form.Control
		      				type="password" 
		      				placeholder="Password" 
		      				value={password}
		      				onChange={e => setPassword(e.target.value)}
		      				required
				/>
		    </Form.Group>

		    {isActive ?
		    <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
		    :
		    <Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
			}
		  </Form>
		);
}
