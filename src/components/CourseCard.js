import{Button, Row, Col, Card} from "react-bootstrap";
import {useState, useEffect} from "react";
import {Link} from 'react-router-dom'

export default function CourseCard({courseProp}){

	console.log(courseProp);
	console.log(typeof courseProp);

	// destructure the object for more code readability and ease of coding for the part of the dec(doesn't have to access multiple objects before accessing the needed properties)
	const {name, description, price, _id} = courseProp;

/*
	use the state hook for this component to be able to store its stable
	States are used to keep track of the information related to individual components

		SYNTAX: const [getter, setter] = useState(initialGetter)
*/
	/*const [count, setCount] = useState(0);
	const [seats, setSeat] = useState(10);
	// create another userState to refactor the enroll function
	const [isOpen, setIsOpen] = useState(true);*/

		/*
			this function keeps track of the enrollees for a course
			the setter function for useStates are asynchronous allowing it to be expected separately from other codes (setCount us executed while the "console.log")
		*/
	/*function enroll(){
			console.log(`Enrollees:${count}`)
			setCount(count+1);
			console.log(`Enrollees:${seats}`)
			setSeat(seats-1)
	}*/

	/*
		effect hook - allows us to execute a piece of code when a component gets rendered or if a value of the state changes3
		useEffect - requires argumanets: function and a dependency array-BOTH ARE NEEDED
			function-to set which lines of codes are to be executed	
			dependency array - identifies which variables/ are to be listened to, in terms of changing the state, for the function to be executed
			 if the array is left empty, the codes will not listen to any variable but the codes will be executed once
	*/
	/*useEffect(()=>{
		if(seats===0){
			setIsOpen(false)
		}
	}, [seats]);*/
	/*
		Two-way binding
			refers to the domino effect of changing the state going into the change of the display/UI in the frontend
	*/


	return(
		<Row className="mt-3 mb-3">
			<Col>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>{name}</Card.Title>

						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>

						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>{price}</Card.Text>

						<Link className = 'btn btn-primary' to={`/courses/${_id}`}>Details</Link>
					</Card.Body>
				</Card>
			</Col>


		</Row>

		)
}