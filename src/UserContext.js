import React from "react";

// creates UserContext object 
/*
	createContext method allows us to store different data inside an object, in this case called UserContext. This set of information can be shared to other  components within the app

	with this method, we will be able to create a global state to store the user details instead of getting if from the localStorage fromt time to time

	a different approach to passing information between components without the use of props and pass it from parent to child component
*/

const UserContext = React.createContext();

/*
	Provier component allows other components to consume/use the context object and supply the necessary information neede to the context object
*/

// export without default needs destructuring before being able to be imported by other components
export const UserProvider = UserContext.Provider;

export default UserContext;